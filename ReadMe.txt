generateur_refs_REC
    base_refs
    |   * xlsx avec infos récupérées des formulaires BRGM à chaque génération de pdf
    |
    formulaires_archives
    |   * formulaires BRGM sont déplacés ici lorsqu'ils ont été lus, et traduits en ref. EU/BM
    |
    formulaires_completes
    |   * dossier qui est surveillé lorsqu'on lance la génération des ref. Une fois lus, les pdf sont déplacés dans archives
    |
    references_generees
        |
        banque_mondiale
        |   * les ref format banque mondiale sont écrites ici
        europe 
        |   * les ref format europe sont écrites ici
    src
    |   * les sources (py) = le code
    |   
    templates
        |
        brgm
        |   le formulaire créé par Peggy pour les ref. au format BRGM (= questionnaire)
        |
        banque_mondiale
        |   * les canevas de références banque mondiale, selon lesquels les refs. seront produites. Ces canevas contiennent les balises (ex: {{num_projet}}) qui seront remplacées par les informations tirées sur formulaire BRGM.
                le nom des fichiers compte (beaucoup) : il faut qu'ils s'appellent banque_mondiale_XX avec XX la langue (FR,ENG,ESP).
                La typo (font, graisse, italique), les couleurs, le fond,surlignage,etc... sont préservés. Il faut donc que le canevas soit exactement tel que l'attendu.
        |   * banque_mondiale_FR.docx
        |   * banque_mondiale_ENG.docx
        |   * banque_mondiale_ESP.docx        
        europe
        |   * les canevas des ref. Europe. MM principe que pour banque mondiale
    