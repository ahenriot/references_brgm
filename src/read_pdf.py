import PyPDF2
# import minecart
# import Pillow
import os
# from io import BytesIO

# main_dir=r"D:\Documents\henriot\OneDrive - BRGM\10_D3E\04_transverse\references_brgm_to_bm"

# main_dir=r"D:\Documents\henriot\BRGM\DEPA EVE - Documents\MONTAGES\generateur_refs_REC"
# forms_dir=os.path.abspath(os.path.join(main_dir,'formulaires_completes'))

def read_brgm_template(forms_dir, file):
    with open(os.path.join(forms_dir,file), 'rb') as file:
        pdf_reader=PyPDF2.PdfFileReader(file)
        data_in_fields=pdf_reader.getFields()
        # txt_in_fields=pdf_reader.getFormTextFields()
    return data_in_fields

#
# with open(r"D:\Documents\henriot\BRGM\DEPA EVE - Documents\MONTAGES\generateur_refs_REC\formulaires_completes\REF_Nom-Projet_Année-fin_FR-ENG_vf(3).pdf", 'rb') as file:
#
#
# pdf_reader=PyPDF2.PdfFileReader(r"D:\Documents\henriot\BRGM\DEPA EVE - Documents\MONTAGES\generateur_refs_REC\formulaires_completes\REF_Nom-Projet_Année-fin_FR-ENG_vf(3).pdf")
# data_in_fields=pdf_reader.getFields()
# return data_in_fields
#
#
# pdffile = open(r"D:\Documents\henriot\BRGM\DEPA EVE - Documents\MONTAGES\generateur_refs_REC\formulaires_completes\001_Indicateurs-secheresse_2017-2020_FR_vf.pdf", 'rb')
# doc = minecart.Document(pdffile)
# for p in [0,1,2,3,4,5]:
#     page = doc.get_page(p) # getting a single page
#     print(page.images)
#
# doc.get_page(0)
# #iterating through all pages
# for page in doc.iter_pages():
#     im = page.images[0].as_pil()  # requires pillow
#     display(im)