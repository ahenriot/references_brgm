import os
import sys
import pandas as pd
import datetime as dt

local=False
if local:
    main_dir=r"D:\Documents\henriot\BRGM\DEPA EVE - Documents\MONTAGES\generateur_refs_REC"
    sys.path.append(main_dir)
    from src.read_pdf import read_brgm_template
    from src.write_word import write_to_word

else:
    main_dir=os.path.abspath(os.path.dirname(os.curdir)) # suppose que src soit un repo de main_dir + que main.py lancé depuis main en tant que /src/main.py
    print(f'main = {main_dir}')
    print(f'{os.listdir()}')
    from read_pdf import read_brgm_template
    from write_word import write_to_word

# main_dir=r"D:\Documents\henriot\OneDrive - BRGM\10_D3E\04_transverse\references_brgm_to_bm"

# main_dir=r"D:\Documents\henriot\BRGM\DEPA EVE - Documents\MONTAGES\generateur_refs_REC"

forms_dir=os.path.abspath(os.path.join(main_dir,'formulaires_completes'))
archiv_dir=os.path.abspath(os.path.join(main_dir,'formulaires_archives'))
table_dir=os.path.abspath(os.path.join(main_dir,'base_refs'))
out_dir=os.path.abspath(os.path.join(main_dir,'references_generees'))
templates_dir=os.path.abspath(os.path.join(main_dir,'templates'))
log_dir=os.path.abspath(os.path.join(main_dir,'logs'))

list_of_ref=[file for file in os.listdir(forms_dir) if file.endswith('.pdf')]

#
# for ref in list_of_ref:
#     fields=read_brgm_template(ref)
#     try :
#         my_fields={key:value['/V'] for key,value in fields.items()}
#     except:
#         my_fields = {key: value['/TU'] for key, value in fields.items()}
#     for my_dir in ['bm','europe']:
#         write_to_word(my_dir,my_fields)
log=f'{dt.datetime.today().isoformat()}\nRun lancé depuis {main_dir}'
print(f'Running...\n(start at :{dt.datetime.today()})')
list_tb=[]
for ref in list_of_ref:
    log=log+'\n'
    fields = read_brgm_template(forms_dir,ref)
    os.replace(os.path.join(forms_dir,ref),os.path.join(archiv_dir,ref))
    my_fields={}
    dico_mapping = {'fr': 'FR', 'Ang': 'ENG', 'Esp': 'ESP', 'ang':'ENG', 'esp':'ESP'}
    ref_mapping={'banque_mondiale':'BM','europe':'EU'}
    # read the infos
    # print(f'{fields.keys()}')
    for key, val in fields.items():
        if '%' in str(key):
            key=str(key).replace('%','pcent')
        # langage
        # keys : Oui, Oui_2, Oui_3

        if key in ['fr','Ang','Esp','ang','esp']:
            # print(f'Clée {key} trouvée')
            # if my_fields.get('langage', None) == ['FR']: # la clee existe, et valeur par défaut
            #     pass
            # else: # la clee n'existe pas
            if '/FT' in val.keys():
                if val['/FT'] in ['/Btn','/Tx']:
                    # print('Valeur est ok ')
                    if '/V' in val.keys(): # le bouton est cliqué, /V porte une valeur
                        # print('cliqué')
                        if my_fields.get('langage', None) is not None:
                            # print(f'ajout de la valeur : {dico_mapping[key]}')
                            # print(my_fields['langage'])
                            my_fields['langage'].append(dico_mapping[key])
                            # print(my_fields['langage'])
                        else:
                            # print('creation')
                            my_fields['langage'] = [dico_mapping[key]]
                            log=log+f'\n la langue du document est manquante ou ambigue, forcage à français'
                            # print(my_fields['langage'])

        if '/FT' in val.keys():
            # if val['/FT'] in ['/Btn','/Tx']:
            if val['/FT'] in ['/Tx']:
                if '/V' in val.keys():
                    if isinstance(val['/V'], bytes):
                        try :
                            val_txt=val['/V'].decode('utf-8', 'strict')
                            log=log+f'\nFichier :\n{ref}\n Champ {key} décodé en utf-8'

                        except:
                            try :
                                val_txt=val['/V'].decode('latin-1', 'strict')
                                log = log + f'\nFichier :\n{ref}\n Champ {key} décodé en latin_1'
                                # print('try -> latin-1')
                                # print(val_txt)
                                # my_fields[key] = val_txt
                            except:
                                val_txt=val['/V']
                                log = log + f'\nFichier :\n/!\ {ref} /!\  -- champ {key}\n decodage des bytestring a échoué'
                                # print('try : le reste')
                                # print(val_txt)
                                # # my_fields[key] = val_txt
                        my_fields[key] = val_txt.replace('\x90','\'')
                    else:
                        my_fields[key]=val['/V']
                else:
                    my_fields[key] = ''
            elif val['/FT'] in ['/Btn']:
                if '/V' in val.keys():
                    my_fields[key]='X'
                else:
                    my_fields[key]=''
# else:
                #     my_fields[key]=val['/V']
            # else:
            #     if val['/FT']=='/Tx':
            #         my_fields[key] = ''
            #     else:
            #         my_fields[key]= None
            else:
                my_fields[key]=val['/FT']
        else:
            pass

    # check for the langage:
    if my_fields.get('langage', None) is None:
        my_fields['langage']=['FR']
    elif len(my_fields.get('langage', None)) >1:
        my_fields['langage'] = ['FR']

    for my_dir in ['banque_mondiale','europe']:
        dir_in = os.path.join(templates_dir, my_dir)
        dir_in=f"{dir_in}\\{my_dir}_{my_fields['langage'][0]}.docx"
        # print(dir_in)
        dir_out=os.path.join(out_dir, my_dir)
        the_name = str(my_fields['num_projet']).strip()
        the_name = the_name.translate(the_name.maketrans({"\\": '_', '-': '_', '/': '_', '%': '_', ' ': '_'}))
        name_of_file = f"{my_fields['langage'][0]}_{ref_mapping[my_dir]}_{the_name}_{dt.datetime.today().isoformat().replace('.', '_').replace(':', '_')}.docx"
        dir_out=f'{dir_out}\\{name_of_file}'
        # print(dir_out)

        write_to_word(dir_in, dir_out,my_fields)

    # write a data sheet with all infos, for the ref added at one time
    my_fields['langage']=str(my_fields['langage'])
    tb_ref=pd.DataFrame(my_fields, index=[0])
    tb_ref.set_index('num_projet')
    list_tb.append(tb_ref)
    # add to the datasheet
if len(list_tb)>0:
    the_one=pd.concat(list_tb, sort=False)
    file_name = f"{os.path.join(table_dir, 'refs_ajout_du_')}_{dt.datetime.isoformat(dt.datetime.today(), '_').replace('.', '_').replace(':', '_')}"
    the_one.to_excel(f'{file_name}.xlsx', sheet_name='references_projets_brgm', encoding='utf-8', index=True)
else:
    print('Oooooooops !\nIt seems there were nothing to write in your database...\nAre you sure you add new pdf file I can work with ?')
    log=log+'No pdf file to work with. Nothing was done.'
log_name=dt.date.today().isoformat()
with open(os.path.join(log_dir,f'log_{log_name}.txt'),'w') as f:
    f.write(log)
print('Done !\nHappy having working for you, and hope to see you soon ! <3')