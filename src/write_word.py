from docxtpl import DocxTemplate
import os
import datetime as dt

# main_dir=r"D:\Documents\henriot\OneDrive - BRGM\10_D3E\04_transverse\references_brgm_to_bm"
# main_dir=r"D:\Documents\henriot\BRGM\DEPA EVE - Documents\MONTAGES\generateur_refs_REC"
# templates_dir=os.path.abspath(os.path.join(main_dir,'templates'))
# out_dir=os.path.abspath(os.path.join(main_dir,'references_generees'))

def write_to_word(dir_in, dir_out,dico_fields):
    #
    # if my_dir == 'banque_mondiale':
    #     if dico_fields['langage'][0] == "FR":
    #         doc=DocxTemplate(os.path.join(templates_dir,my_dir,'banque_mondiale_FR.docx'))
    #     elif dico_fields['langage'][0] == "ENG":
    #         doc=DocxTemplate(os.path.join(templates_dir,my_dir,'banque_mondiale_ENG.docx'))
    #     elif dico_fields['langage'][0] == "ESP":
    #         doc=DocxTemplate(os.path.join(templates_dir,my_dir,'banque_mondiale_ESP.docx'))
    #     else:
    #         doc = DocxTemplate(os.path.join(templates_dir, my_dir, 'banque_mondiale_FR.docx'))
    # if my_dir =='europe':
    #     doc = DocxTemplate(os.path.join(templates_dir, my_dir, 'europe.docx'))

    doc=DocxTemplate(dir_in)

    try :
        doc.render(dico_fields)
    except :
        # context = {'name': 'gandi', 'surname': 'mahatma', 'tel_num': '0656789102'}
        # doc.render(context)
        print('Erreur lors du parsing vers les templates word')
    # the_name=str(dico_fields['nom_projet']).strip()
    # the_name=the_name.translate(the_name.maketrans({"\\":'_','-':'_','/':'_','%':'_',' ':'_'}))
    # name_of_file=f"{dico_fields['langage']}_{the_name}_{dt.datetime.today().isoformat().replace('.','_').replace(':','_')}"
    try:
        doc.save(dir_out)
    except:
        # doc.save(os.path.join(dir_out, f"OUT_{dt.datetime.today().isoformat().replace('.','_').replace(':','_')}.docx"))
        print('Erreur lors de l ecriture du doc.')
